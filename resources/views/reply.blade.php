@extends('layouts.master')

@section('contentholder')
<div class="col-md-12">

	<form role="form" method="POST" action="{{ url('/send') }}">
		{{ csrf_field() }}
		<table>
			<tr>
				<th>{{ __('Reply To Message') }}</th>
			</tr>
			<tr>
				<td>
					<input
					class="form-control{{ $errors->has('to') ? ' is-invalid' : '' }}"
					type="text" id="to" name="to" placeholder="Recipients"
					required="required" value="{{ $request->from }}">
				</td>
			</tr>
			<tr>
				<td><input class="form-control" type="text" id="subject"
					name="subject" placeholder="Subject" value="{{ 'Re: '.$request->subject }}"></td>
			</tr>
			<tr>
				<td><textarea class="form-control" id="mail_text" name="mail_text" rows=15 cols="500">{{ "\n \n".'Subject: '.$request->subject."\nTo: ".$request->to."\n\n".$request->mail_text }}</textarea></td>
			</tr>
			<tr>
				<td>
					<button id="forward" name="forward" type="submit" class="btn btn-primary">{{
						__('Send') }}</button>
				</td>
			</tr>
			<tr>
				<td>
					<input type="hidden" id="id" name="id" value="{{ $request->id }}">
					<input type="hidden" id="type" name="type" value="{{ $request->type }}">
				</td>
			</tr>
		</table>
		<input type="hidden" id="from" name="from"
			value="{{ Auth::user()->email }}" />
	</form>
</div>
@endsection

<script src="{{ asset('js/jquery-1.11.1.min.js') }}"></script>
<script type="text/javascript">

</script>