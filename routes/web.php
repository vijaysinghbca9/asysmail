<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/inbox', 'InboxController@index')->name('home');
Route::get('/drafts', 'DraftsController@index')->name('drafts');
Route::get('/sent', 'SentController@index')->name('sent');
Route::get('/trash', 'TrashController@index')->name('trash');

Route::get('/inbox/delete/{mail_id}', 'InboxController@delete')->name('home');
Route::get('/drafts/delete/{mail_id}', 'DraftsController@delete')->name('drafts');
Route::get('/sent/delete/{mail_id}', 'SentController@delete')->name('sent');
Route::get('/trash/delete/{type}/{mail_id}', 'TrashController@delete')->name('trash');

Route::get('/inbox/{id}', 'InboxController@get');
Route::get('/sent/{id}', 'SentController@get');
Route::get('/drafts/{id}', 'DraftsController@get');
Route::get('/trash/{type}/{id}', 'TrashController@get');

Route::get('/compose', function(){ return view('compose'); });
Route::post('/send', array('uses'=>'SendMailController@SendMail'));
Route::post('/forwardreply', array('uses'=>'SendMailController@ForwardReply'));
