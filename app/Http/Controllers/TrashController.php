<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use GuzzleHttp;

class TrashController extends Controller
{
    public function index() {
        
        $client = new GuzzleHttp\Client();
        $response = $client->post('http://localhost/smtp/api/trash',
            ['form_params' => [
                'email' => Auth::user()->email
            ]]);
        
        $body = $response->getBody()->getContents();
        $body = (array)json_decode($body);
        
        return View::make('trash')->with('trashMails', $body);
    }
    
    public function delete($type, $mail_id) {
        
        $client = new GuzzleHttp\Client();
        $response = $client->post('http://localhost/smtp/api/trash/delete',
            ['form_params' => [
                'email' => Auth::user()->email,
                'id' => $mail_id,
                'type' => $type
            ]]);
        
        $body = $response->getBody()->getContents();
        $body = (array)json_decode($body);
        
        return View::make('trash')->with('trashMails', $body);
    }
    
    public function get($type, $id) {
        
        $client = new GuzzleHttp\Client();
        $url = 'http://localhost/smtp/api/trash/inbox/get';
        
        if ($type == 'sent')
            $url = 'http://localhost/smtp/api/trash/sent/get';
        
        $response = $client->post($url,
            ['form_params' => [
                'email' => Auth::user()->email,
                'id' => $id
            ]]);
        
        $body = $response->getBody()->getContents();
        $body = (array)json_decode($body);
        
        return View::make('read')->with('mail', $body);
    }
}
