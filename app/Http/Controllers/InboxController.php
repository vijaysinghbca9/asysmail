<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use GuzzleHttp;

class InboxController extends Controller
{
    public function index() {
        
        $client = new GuzzleHttp\Client();
        $response = $client->post('http://localhost/smtp/api/inbox',
            ['form_params' => [
                'email' => Auth::user()->email
            ]]);
        
        $body = $response->getBody()->getContents();
        $body = (array)json_decode($body);
        
        return View::make('inbox')->with('inboxMails', $body);
    }
    
    public function delete($mail_id) {
        
        $client = new GuzzleHttp\Client();
        $response = $client->post('http://localhost/smtp/api/inbox/delete',
            ['form_params' => [
                'email' => Auth::user()->email,
                'id' => $mail_id
            ]]);
        
        $body = $response->getBody()->getContents();
        $body = (array)json_decode($body);
        
        return View::make('inbox')->with('inboxMails', $body);
    }
    
    public function get($id) {
        
        $client = new GuzzleHttp\Client();
        $response = $client->post('http://localhost/smtp/api/inbox/get',
            ['form_params' => [
                'email' => Auth::user()->email,
                'id' => $id
            ]]);
        
        $body = $response->getBody()->getContents();
        $body = (array)json_decode($body);
        
        return View::make('read')->with('mail', $body);
    }
}
