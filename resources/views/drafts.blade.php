@extends('layouts.master')

@section('contentholder')

<div class="col-md-12">
	<table class="card" cellpadding="5">
		<thead>
			<tr>
				<th>&nbsp;</th>
				<th>&nbsp;</th>
				<th>To</th>
				<th>Subject</th>
				<th>&nbsp;</th>
				<th>Body</th>
			</tr>
		</thead>
		<tbody>
			@if (isset($drafts))
				@foreach($drafts as $draft)
					@if (isset($draft) && isset($draft->id))
                     	@if (isset($draft->is_unread) && $draft->is_unread == 1)
            			<tr>
            				<td><a href="{{url('/drafts/').'/'.$draft->id}}">read</a></td>
            				<td><a href="{{url('/drafts/delete/').'/'.$draft->id}}">delete</a></td>
            				<td><b> {{$draft->to}} </b></td>
            				<td><b> {{$draft->subject}} </b></td>
            				<td>-</td>
            				<td>{{$draft->mail_text}}</td>
            			</tr>
        			@else
            			<tr>
            				<td><a href="{{url('/drafts/').'/'.$draft->id}}">read</a></td>
            				<td><a href="{{url('/drafts/delete/').'/'.$draft->id}}">delete</a></td>
            				<td>{{$draft->to}}</td>
            				<td>{{$draft->subject}}</td>
            				<td>-</td>
            				<td>{{$draft->mail_text}}</td>
            			</tr>
					@endif
				@endif
			@endforeach
		@endif
		</tbody>
	</table>
</div>
@endsection
