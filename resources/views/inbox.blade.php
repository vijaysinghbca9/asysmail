@extends('layouts.master')

@section('contentholder')

<div class="col-md-12" style="width: 95%;">
	<table class="card" cellpadding="5">
    <thead>
        <tr>
            <th>&nbsp;</th>
            <th>&nbsp;</th>
            <th> From</th>
            <th> Subject </th>
            <th>&nbsp;</th>
            <th> Body</th>
        </tr>
    </thead>
    <tbody>
    	@if (isset($inboxMails))
			@foreach($inboxMails as $inboxMail)
             	@if (isset($inboxMail) && isset($inboxMail->id))
                 	@if (isset($inboxMail->is_unread) && $inboxMail->is_unread == 1)
                      <tr>
                          <td><a href="{{url('/inbox/').'/'.$inboxMail->id}}">read</a></td>
                          <td><a href="{{url('/inbox/delete/').'/'.$inboxMail->id}}">delete</a></td>
                          <td><b> {{$inboxMail->from}} </b></td>
                          <td><b> {{$inboxMail->subject}} </b></td>
                          <td>-</td>
                          <td> {{$inboxMail->mail_text}} </td>
                          <td><input type="hidden" id="mail_id" name="mail_id" value="{{$inboxMail->id}}"></td>
                      </tr>
                    @else
                    	<tr>
                          <td><a href="{{url('/inbox/').'/'.$inboxMail->id}}">read</a></td>
                          <td><a href="{{url('/inbox/delete/').'/'.$inboxMail->id}}">delete</a></td>
                          <td> {{$inboxMail->from}} </td>
                          <td> {{$inboxMail->subject}} </td>
                          <td>-</td>
                          <td> {{$inboxMail->mail_text}} </td>
                          <td><input type="hidden" id="mail_id" name="mail_id" value="{{$inboxMail->id}}"></td>
                      </tr>
                  	@endif
                 @endif
             @endforeach
         @endif
   </tbody>
</table>
</div>
@endsection