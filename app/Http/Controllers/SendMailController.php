<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use GuzzleHttp;

class SendMailController extends Controller
{
    public function SendMail(Request $request)
    {
        $client = new GuzzleHttp\Client();
        $response = $client->post('http://localhost/smtp/api/send', 
            ['form_params' => [
            'to' => $request->to,
            'from' => $request->from,
            'subject' => $request->subject,
            'mail_text' => $request->mail_text
        ]]);
        
        $body = $response->getBody()->getContents();
        //$body = json_decode($body);

        return View::make('response')->with('response', $body);
    }
    
    public function ForwardReply(Request $request)
    {
        if ($request->type == "inbox")
        {
            return $this->forward($request);
        }
        else if ($request->type == "sent")
        {
            return $this->reply($request);
        }
        else if ($request->type == "draft")
        {
            return $this->sendDraft($request);
        }
    }

    private function forward(Request $request)
    {
        return View::make('forward')->with('request', $request);
    }
    
    private function reply(Request $request)
    {
        return View::make('reply')->with('request', $request);
    }
    
    private function sendDraft(Request $request)
    {
        return View::make('draft')->with('request', $request);
    }
}
