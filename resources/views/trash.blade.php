@extends('layouts.master')

@section('contentholder')

<div class="col-md-12" style="width: 95%;">
	<table class="card" cellpadding="5">
		<thead>
			<tr>
				<th>&nbsp;</th>
				<th>&nbsp;</th>
				<th>To</th>
				<th>Subject</th>
				<th>&nbsp;</th>
				<th>Body</th>
			</tr>
		</thead>
		<tbody>
			@if (isset($trashMails))
				@foreach($trashMails as $trashMail)
					@if (isset($trashMail) && isset($trashMail->is_unread) && $trashMail->is_unread == 1)
						@if (isset($trashMail->to))
                			<tr>
                				<td><a href="{{url('/trash/sent').'/'.$trashMail->id}}">read</a></td>
                				<td><a href="{{url('/trash/delete/sent').'/'.$trashMail->id}}">delete</a></td>
                				<td><b> {{$trashMail->to}} </b></td>
                				<td><b> {{$trashMail->subject}} </b></td>
                				<td>-</td>
                				<td>{{$trashMail->mail_text}}</td>
                				<td><input type="hidden" id="type" name="type" value="sent"></td>
                			</tr>
            			@else
            				<tr>
                				<td><a href="{{url('/trash/inbox').'/'.$trashMail->id}}">read</a></td>
                				<td><a href="{{url('/trash/delete/inbox').'/'.$trashMail->id}}">delete</a></td>
                				<td><b> {{$trashMail->from}} </b></td>
                				<td><b> {{$trashMail->subject}} </b></td>
                				<td>-</td>
                				<td>{{$trashMail->mail_text}}</td>
                				<td><input type="hidden" id="type" name="type" value="inbox"></td>
                			</tr>
            			@endif
        			@else
        				@if (isset($trashMail->to))
                			<tr>
                				<td><a href="{{url('/trash/sent').'/'.$trashMail->id}}">read</a></td>
                				<td><a href="{{url('/trash/delete/sent').'/'.$trashMail->id}}">delete</a></td>
                				<td>{{$trashMail->to}}</td>
                				<td>{{$trashMail->subject}}</td>
                				<td>-</td>
                				<td>{{$trashMail->mail_text}}</td>
                				<td><input type="hidden" id="type" name="type" value="sent"></td>
                			</tr>
            			@else
            				<tr>
                				<td><a href="{{url('/trash/inbox').'/'.$trashMail->id}}">read</a></td>
                				<td><a href="{{url('/trash/delete/inbox').'/'.$trashMail->id}}">delete</a></td>
                				<td>{{$trashMail->from}}</td>
                				<td>{{$trashMail->subject}}</td>
                				<td>-</td>
                				<td>{{$trashMail->mail_text}}</td>
                				<td><input type="hidden" id="type" name="type" value="inbox"></td>
                			</tr>
            			@endif
					@endif
				@endforeach
			@endif
		</tbody>
	</table>
</div>
@endsection
