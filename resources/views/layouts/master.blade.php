@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Mails</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    
					<table style="">
						<tr>
							<td style="vertical-align: top;">
								<table>
                                	<tr>
                                		<td>
                                			<a class="btn btn-primary" href="{{ url('/compose') }}">Compose</a>
                                		</td>
                                	</tr>
                                	<tr>
                                		<td><a href="{{ url('/inbox') }}">Inbox</a></td>
                                	</tr>
                                	<tr>
                                		<td><a href="{{ url('/drafts') }}">Drafts</a></td>
                                	</tr>
                                	<tr>
                                		<td><a href="{{ url('/sent') }}">Sent</a></td>
                                	</tr>
                                	<tr>
                                		<td><a href="{{ url('/trash') }}">Trash</a></td>
                                	</tr>
                                </table>
							</td>
							<td>
								<table>
									<tr>
    									<td>
    										@yield('contentholder')
    									</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
