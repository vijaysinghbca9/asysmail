@extends('layouts.master') @section('contentholder')
<div class="col-md-12">
	<form role="form" method="POST" action="{{ url('/send') }}">
		{{ csrf_field() }}
		<table>
			<tr>
				<th>{{ __('New Message') }}</th>
			</tr>
			<tr>
				<td><input
					class="form-control{{ $errors->has('to') ? ' is-invalid' : '' }}"
					type="text" id="to" name="to" placeholder="Recipients"
					required="required">
				
				<td>@if ($errors->has('to')) <span class="invalid-feedback"> <strong>{{
							$errors->first('to') }}</strong>
				</span> @endif
			
			</tr>
			<tr>
				<td><input class="form-control" type="text" id="subject"
					name="subject" placeholder="Subject"></td>
			</tr>
			<tr>
				<td><textarea class="form-control" id="mail_text" name="mail_text"
						rows=15 cols="500"></textarea></td>
			</tr>
			<tr>
				<td>
					<button id="submit" type="submit" class="btn btn-primary">{{
						__('Send') }}</button>
				</td>
			</tr>
		</table>
		<input type="hidden" id="from" name="from"
			value="{{ Auth::user()->email }}" />
	</form>
</div>
@endsection

<script src="{{ asset('js/jquery-1.11.1.min.js') }}"></script>
<script type="text/javascript">
$(document).ready(function(){
	$(window).on('unload', function(){
	    saveDraft();
	});
});

function saveDraft() {
	$.ajax({
        type: 'POST',
        url: 'http://localhost/smtp/api/drafts/save',
        async:false,
        data: {
         'from':$('#from').val(),
         'to':$('#to').val(),
         'subject':$('#subject').val(),
         'mail_text':$('#mail_text').val()
         },
    }).done(function(data) {                
        alert(data);
    });
}
</script>