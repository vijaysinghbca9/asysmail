@extends('layouts.master')

@section('contentholder')
<div class="col-md-12">

	<form role="form" method="POST" action="{{ url('/forwardreply') }}">
		{{ csrf_field() }}
		<table>
			<tr>
				<th>{{ __('Message') }}</th>
			</tr>
			<tr>
				<td><label class="form-control{{ $errors->has('to') ? ' is-invalid' : '' }}" id="to">{{ $mail[0]->to }}</label></td>
			</tr>
			<tr>
				<td><label class="form-control" id="subject">{{ $mail[0]->subject }}</label></td>
			</tr>
			<tr>
				<td><textarea class="form-control" id="mail_text" rows=15 cols="500" readonly="readonly" disabled="disabled">{{ $mail[0]->mail_text }}</textarea></td>
			</tr>
			<tr>
				<td>
					<button id="forward" name="forward" type="submit" class="btn btn-primary">{{
						__('Forward') }}</button>
					<button id="reply" name="reply" type="submit" class="btn btn-primary">{{
						__('Reply') }}</button>
				</td>
			</tr>
			<tr>
				<td>
					<input type="hidden" id="id" name="id" value="{{ $mail[0]->id }}">
					<input type="hidden" id="type" name="type" value="{{ $mail[0]->type }}">
					<input type="hidden" id="to" name="to" value="{{ $mail[0]->to }}">
					<input type="hidden" id="subject" name="subject" value="{{ $mail[0]->subject }}">
					<input type="hidden" id="mail_text" name="mail_text" value="{{ $mail[0]->mail_text }}">
				</td>
			</tr>
		</table>
		<input type="hidden" id="from" name="from"
			value="{{ Auth::user()->email }}" />
	</form>
</div>
@endsection

<script src="{{ asset('js/jquery-1.11.1.min.js') }}"></script>
<script type="text/javascript">
//$(document).ready(function(){alert('hi');});

// $('#submit').click(function(e){
// 	e.preventDefault();

//     $.ajax({
//         type: "POST",
//         url: "http://localhost/smtp/api/send",
//         dataType: "html",
//        	data: {
//            'from':$('input[name=from]').val(),
//            'to':$('input[name=to]').val(),
//            'subject':$('input[name=subject]').val(),
//            'mail_text':$('input[name=mail_text]').val()
//            },
//         success: function(response) {
//             alert(response[0]);
//           //$('#my_div').html(response);
//         }
//       });

//     });
</script>