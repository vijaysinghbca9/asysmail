@extends('layouts.master')

@section('contentholder')

<div class="col-md-12" style="width: 95%;">
	<table class="card" cellpadding="5">
		<thead>
			<tr>
				<th>&nbsp;</th>
				<th>&nbsp;</th>
				<th>To</th>
				<th>Subject</th>
				<th>&nbsp;</th>
				<th>Body</th>
			</tr>
		</thead>
		<tbody>
			@if (isset($sentMails))
				@foreach($sentMails as $sentMail)
					@if (isset($sentMail) && isset($sentMail->id))
                     	@if (isset($sentMail->is_unread) && $sentMail->is_unread == 1)
                			<tr>
                				<td><a href="{{url('/sent/').'/'.$sentMail->id}}">read</a></td>
                				<td><a href="{{url('/sent/delete/').'/'.$sentMail->id}}">delete</a></td>
                				<td><b> {{$sentMail->to}} </b></td>
                				<td><b> {{$sentMail->subject}} </b></td>
                				<td>-</td>
                				<td>{{$sentMail->mail_text}}</td>
                			</tr>
            			@else
                			<tr>
                				<td><a href="{{url('/sent/').'/'.$sentMail->id}}">read</a></td>
                				<td><a href="{{url('/sent/delete/').'/'.$sentMail->id}}">delete</a></td>
                				<td>{{$sentMail->to}}</td>
                				<td>{{$sentMail->subject}}</td>
                				<td>-</td>
                				<td>{{$sentMail->mail_text}}</td>
                			</tr>
    					@endif
					@endif
				@endforeach
			@endif
		</tbody>
	</table>
</div>
@endsection
